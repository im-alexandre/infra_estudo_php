<?php 
$pdo = new PDO("pgsql:host={$_ENV['HOST']};
                port={$_ENV['PORT']};
                dbname={$_ENV['DBNAME']};
                user={$_ENV['DBUSER']};
                password={$_ENV['PASSWORD']}");

/*
 * Exemplo de código vulnerável a SQL injection
 */
//$query="select * from tabela_teste where nome = {$_POST['usuario']} and 0=1;";
//$consulta = $pdo->query($query);


# Utilizando o prepare para evitar o SQL injection
$query="select * from tabela_teste where nome = :nome and 0=1;";
$usuario = $_POST['usuario'];
$consulta = $pdo->prepare($query);
$consulta->execute(['nome'=>$usuario]);



echo $consulta->queryString . "<br>";


echo "<html><body>";
echo "<h1>Resultado da pesquisa</h1>";
    
echo "<ul>";
while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)){
    echo "<li><h2>".$linha['id']." ".$linha['nome']."</li></h2>";    
}
echo "</ul>";
echo "</body> </html>";

?>
