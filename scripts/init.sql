drop table if exists public.tabela_funcao;
drop table if exists public.tabela_teste;
create table tabela_teste(
	"id" serial primary key,
    "nome" varchar
);

CREATE TABLE public.tabela_funcao
(
    id serial primary key,
    id_func integer,
    funcao character varying,
	CONSTRAINT fk_func
      FOREIGN KEY(id_func) 
	  REFERENCES tabela_teste(id)
);

ALTER TABLE public.tabela_funcao
    OWNER to postgres;

insert into public.tabela_teste (nome) values ('Alexandre');
insert into public.tabela_teste (nome) values ('João');


insert into public.tabela_funcao (id_func, funcao) values (1, 'desenvolvedor');
insert into public.tabela_funcao (id_func, funcao) values (2, 'dba');

-- insert into public.tabela_funcao (id_func, funcao) values (1, 'devops');